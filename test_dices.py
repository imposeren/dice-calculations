# -*- coding: utf-8 -*-

from __future__ import division

import unittest
import numpy as np

import calc_dice


class DicesTestCase(unittest.TestCase):

    def test_all(self):
        res_2dice = calc_dice.simple_rolls(2)
        res_3dice = calc_dice.simple_rolls(3)
        assert len(res_2dice) == 6 * 6
        assert len(res_3dice) == 6 * 6 * 6

        def prolems_on_ones_gte_probability(risk, num_problems):
            return calc_dice.count_condition_probability(
                lambda v: v == 1,
                lambda count: count >= num_problems,
                'count(v==1)',
                'val>=%s' % num_problems,
                risk
            )

        def prolems_on_ones_exact_probability(risk, num_problems):
            return calc_dice.count_condition_probability(lambda v: v == 1, lambda count: count == num_problems, 'count(v==1)', 'val==%s' % num_problems, risk)

        self.assertAlmostEqual(prolems_on_ones_gte_probability(1, 1), 1. / 6)

        self.assertAlmostEqual(prolems_on_ones_gte_probability(2, 1), 11. / 36)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(2, 2), 1. / 36)

        self.assertAlmostEqual(prolems_on_ones_gte_probability(3, 1), 91. / 216)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(3, 2), 16. / 216)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(3, 3), 1. / 216)

        self.assertAlmostEqual(prolems_on_ones_gte_probability(4, 1), 671. / 1296)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(4, 2), 171. / 1296)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(4, 3), 21. / 1296)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(4, 4), 1. / 1296)

        self.assertAlmostEqual(prolems_on_ones_gte_probability(5, 1), 4651. / 7776)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(5, 2), 1526. / 7776)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(5, 3), 276. / 7776)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(5, 4), 26. / 7776)
        self.assertAlmostEqual(prolems_on_ones_gte_probability(5, 5), 1. / 7776)

        exploded_data, exploded_probs = calc_dice.complex_rolls(1, explode_on=[6], explode_depth=1)
        chain_exploded_data, chain_exploded_probs = calc_dice.complex_rolls(
            1, explode_on=[6], explode_depth=2)
        items_asserter = getattr(self, 'assertCountEqual', getattr(self, 'assertItemsEqual', None))
        items_asserter(
            chain_exploded_data.tolist(),
            [
                [0., 0., 1.],
                [0., 0., 2.],
                [0., 0., 3.],
                [0., 0., 4.],
                [0., 0., 5.],
                [6., 0., 1.],
                [6., 0., 2.],
                [6., 0., 3.],
                [6., 0., 4.],
                [6., 0., 5.],
                [6., 6., 1.],
                [6., 6., 2.],
                [6., 6., 3.],
                [6., 6., 4.],
                [6., 6., 5.],
                [6., 6., 6.]
            ]
        )

        alternative_chain_exploded_data = calc_dice.multi_pass_exploding_rolls(
            1, 6, lambda x: x == 6, explode_depth=2)
        alternative_probs = calc_dice.calc_probabilities(alternative_chain_exploded_data)

        target_probs = [1] * 5 + [1. / 6] * 5 + [1. / 6 / 6] * 6
        for actual_prob, target_prob in zip(chain_exploded_probs, target_probs):
            self.assertAlmostEqual(actual_prob, target_prob)

        for actual_prob, target_prob in zip(alternative_probs, target_probs):
            self.assertAlmostEqual(actual_prob, target_prob / 6)

        self.assertEqual(len(exploded_data), 5 + 6)
        self.assertAlmostEqual(np.sum(exploded_probs), 6)

        self.assertEqual(len(chain_exploded_data), 5 + 5 + 6)
        self.assertAlmostEqual(np.sum(chain_exploded_probs), 6)

        for data, probs in ((exploded_data, exploded_probs), (alternative_chain_exploded_data, alternative_probs)):

            self.assertAlmostEqual(
                calc_dice.sum_condition_probability(
                    lambda sum: sum == 5, 'sum==5',
                    roll_results=data, probs=probs,
                ),
                1. / 6
            )
            self.assertAlmostEqual(
                calc_dice.sum_condition_probability(
                    lambda sum: sum == 6, 'sum==6',
                    roll_results=data, probs=probs,
                ),
                0
            )
            self.assertAlmostEqual(
                calc_dice.sum_condition_probability(
                    lambda sum: sum == 7, 'sum==7',
                    roll_results=data, probs=probs,
                ),
                1. / 6 / 6
            )
            self.assertAlmostEqual(
                calc_dice.sum_condition_probability(
                    lambda sum: sum == 8, 'sum==8',
                    roll_results=data, probs=probs,
                ),
                1. / 6 / 6
            )

        two_dice_exploded_data, two_dice_exploded_probs = calc_dice.complex_rolls(
            2, explode_on=[6], explode_depth=1)
        self.assertAlmostEqual(np.sum(two_dice_exploded_probs), 36)
        alternative_data = calc_dice.multi_pass_exploding_rolls(
            2, 6, lambda x: x == 6, explode_depth=1)
        alternative_probs = calc_dice.calc_probabilities(alternative_data)

        for data, probs in ((two_dice_exploded_data, two_dice_exploded_probs), (alternative_data, alternative_probs)):
            self.assertAlmostEqual(
                calc_dice.sum_condition_probability(
                    lambda sum: sum == 12, 'sum==12',
                    roll_results=data, probs=probs,
                ),
                10. / 6 / 6 / 6,
                places=6
            )

        data_2d3_explode_twice = calc_dice.multi_pass_exploding_rolls(
            2, 3, lambda x: x == 3, explode_depth=2)
        items_asserter(
            data_2d3_explode_twice.tolist(),
            [
                [1, 1, 0, 0, 0, 0],
                [1, 2, 0, 0, 0, 0],
                [1, 3, 1, 0, 0, 0],
                [1, 3, 2, 0, 0, 0],
                [1, 3, 3, 0, 1, 0],
                [1, 3, 3, 0, 2, 0],
                [1, 3, 3, 0, 3, 0],
                [2, 1, 0, 0, 0, 0],
                [2, 2, 0, 0, 0, 0],
                [2, 3, 1, 0, 0, 0],
                [2, 3, 2, 0, 0, 0],
                [2, 3, 3, 0, 1, 0],
                [2, 3, 3, 0, 2, 0],
                [2, 3, 3, 0, 3, 0],
                [3, 1, 1, 0, 0, 0],
                [3, 1, 2, 0, 0, 0],
                [3, 1, 3, 0, 1, 0],
                [3, 1, 3, 0, 2, 0],
                [3, 1, 3, 0, 3, 0],
                [3, 2, 1, 0, 0, 0],
                [3, 2, 2, 0, 0, 0],
                [3, 2, 3, 0, 1, 0],
                [3, 2, 3, 0, 2, 0],
                [3, 2, 3, 0, 3, 0],
                [3, 3, 1, 1, 0, 0],
                [3, 3, 1, 2, 0, 0],
                [3, 3, 1, 3, 1, 0],
                [3, 3, 1, 3, 2, 0],
                [3, 3, 1, 3, 3, 0],
                [3, 3, 2, 1, 0, 0],
                [3, 3, 2, 2, 0, 0],
                [3, 3, 2, 3, 1, 0],
                [3, 3, 2, 3, 2, 0],
                [3, 3, 2, 3, 3, 0],
                [3, 3, 3, 1, 1, 0],
                [3, 3, 3, 1, 2, 0],
                [3, 3, 3, 1, 3, 0],
                [3, 3, 3, 2, 1, 0],
                [3, 3, 3, 2, 2, 0],
                [3, 3, 3, 2, 3, 0],
                [3, 3, 3, 3, 1, 1],
                [3, 3, 3, 3, 1, 2],
                [3, 3, 3, 3, 1, 3],
                [3, 3, 3, 3, 2, 1],
                [3, 3, 3, 3, 2, 2],
                [3, 3, 3, 3, 2, 3],
                [3, 3, 3, 3, 3, 1],
                [3, 3, 3, 3, 3, 2],
                [3, 3, 3, 3, 3, 3],
            ]
        )

        def prolems_on_one_two_gte_probability(risk, num_problems):
            return calc_dice.on_reduced_probability(
                risk,
                calc_dice.PicklableLambda(
                    lambda data: np.sum(np.rint(data <= 2), axis=1), 'count(val<=2)'),
                calc_dice.PicklableLambda(
                    lambda val: val >= num_problems, 'val>=%s' % num_problems),
            )

        def prolems_on_one_two_exact_probability(risk, num_problems):
            return calc_dice.on_reduced_probability(
                risk,
                calc_dice.PicklableLambda(
                    lambda data: np.sum(np.rint(data <= 2), axis=1), 'count(val<=2)'),
                calc_dice.PicklableLambda(
                    lambda val: val == num_problems, 'val==%s' % num_problems),
            )

        self.assertAlmostEqual(prolems_on_one_two_exact_probability(1, 0), 4. / 6)  # 66.7%
        self.assertAlmostEqual(prolems_on_one_two_exact_probability(1, 1), 2. / 6)  # 33.3%

        self.assertAlmostEqual(prolems_on_one_two_exact_probability(2, 0), 16. / 36)
        self.assertAlmostEqual(prolems_on_one_two_gte_probability(2, 1), 20. / 36)
        self.assertAlmostEqual(prolems_on_one_two_gte_probability(2, 2), 4. / 36)

        self.assertAlmostEqual(prolems_on_one_two_exact_probability(3, 0), 64. / 216)
        self.assertAlmostEqual(prolems_on_one_two_gte_probability(3, 1), 152. / 216)
        self.assertAlmostEqual(prolems_on_one_two_gte_probability(3, 2), 56. / 216)
        self.assertAlmostEqual(prolems_on_one_two_gte_probability(3, 3), 8. / 216)
