# -*- coding: utf-8 -*-
"""This module provides functions to calculate probabilities for different combinations of dice.

Note: calculations use numpy module that will run noticably faster if you install openblas or atlas
before installing numpy (example install for ubuntu: `sudo apt-get install libopenblas-dev`)
"""

from __future__ import division

import itertools
import sys

try:
    import numpy as np
except Exception:
    if sys.argv == ['-c', 'egg_info']:
        class Fake(object):
            pass
        np = Fake()
        np.uint8 = Fake()
        np.sum = Fake()
        np.prod = Fake()
    else:
        raise

__author__ = "Yaroslav Klyuyev <me@imposeren.org>"
__version__ = '0.0.1'


# fake profile when kernprof is not used
try:
    profile
except Exception:
    def profile(func):
        return func

# numexpr is currently slower for tested data
USE_NUMEXPR = False


try:
    import numexpr as ne
except ImportError:
    USE_NUMEXPR = False
    ne = None


def json_default(obj):
    if isinstance(obj, np.ufunc):
        return {'func': unicode(np.ufunc)}
    elif hasattr(obj, 'for_json'):
        return obj.for_json()
    try:
        iterable = iter(obj)
    except TypeError:
        pass
    else:
        return list(obj)
    raise TypeError


_CARTESIAN_CACHE = {}


def set_operation_descriptor(obj, override_when_not_set=None, fallback=repr):
    """Build unique descriptor that identifies object"""
    operation_descriptor = getattr(obj, 'operation_descriptor', '')
    is_lambda = False

    if not operation_descriptor and override_when_not_set:
        operation_descriptor = override_when_not_set

    if not operation_descriptor:
        operation_descriptor = getattr(obj, '__qualname__', '')

    if not operation_descriptor:
        # try processing as module member
        mod_name = getattr(obj, '__module__', '')
        name = getattr(obj, '__name__', '')

        if name == '<lambda>':
            if not fallback or fallback in (repr, str):
                raise TypeError("get_object_operation_descriptor can't guess descriptor for lambdas")
            name = ''

        if mod_name and name:
            operation_descriptor = mod_name + '.' + name

    if not (operation_descriptor or is_lambda):
        # try processing as method
        cls_name = obj
        for attr_name in ('__self__', '__class__', '__name__'):
            if attr_name == '__class__' and isinstance(cls_name, type):
                # skip for class methods
                continue
            cls_name = getattr(cls_name, attr_name, None)
            if cls_name is None:
                break
        name = getattr(obj, '__name__', '')
        if cls_name and name:
            operation_descriptor = cls_name + '.' + name

    if not operation_descriptor:
        if callable(fallback):
            operation_descriptor = fallback(obj)
        elif fallback:
            operation_descriptor = fallback
        else:
            raise RuntimeError("Can't determine descriptor for %r" % obj)
    obj.operation_descriptor = operation_descriptor
    return operation_descriptor



if USE_NUMEXPR:
    def array_sum(array, axis=None, dtype=None):
        if USE_NUMEXPR:
            if not array.size:
                return 0
            return ne.evaluate('sum(array, axis=%s)' % axis)
else:
    array_sum = np.sum

array_sum.operation_descriptor = 'sum'


if USE_NUMEXPR:
    def array_prod(array, axis=None):
        if not array.size:
            return 1.0
        return ne.evaluate('prod(array, axis=%s)' % axis)
else:
    array_prod = np.prod

array_prod.operation_descriptor = 'prod'


def cartesian(arrays, out=None):
    """Generate a cartesian product of input arrays.

    Parameters
    ----------
    arrays : list of array-like
        1-D arrays to form the cartesian product of.
    out : ndarray
        Array to place the cartesian product in.

    Returns
    -------
    out : ndarray
        2-D array of shape (M, len(arrays)) containing cartesian products
        formed of input arrays.
    """
    arrays = [np.asarray(x) for x in arrays]
    dtype = arrays[0].dtype

    key = (x.tostring() for x in arrays)
    if key not in _CARTESIAN_CACHE:
        n = np.prod([x.size for x in arrays])
        if out is None:
            out = np.zeros([n, len(arrays)], dtype=dtype)

        m = n / arrays[0].size
        out[:, 0] = np.repeat(arrays[0], m)
        if arrays[1:]:
            cartesian(arrays[1:], out=out[0:m, 1:])
            for j in range(1, arrays[0].size):
                out[j*m:(j+1)*m, 1:] = out[0:m, 1:]
        _CARTESIAN_CACHE[key] = out
    else:
        out = _CARTESIAN_CACHE[key]

    return out.copy()


class RollOptions(object):
    explode = 1
    chain_explodes = 1 << 1
    reroll = 1 << 2
    chain_rerolls = 1 << 3
    explode_rerolls = 1 << 4


_rolls_cache = {}
_die_dtype = np.uint8


class PicklableLambda(object):
    """Wrapper to make callable usable as key for cache"""

    def __init__(self, function, description):
        self.function = function
        self.operation_descriptor = description

    def __call__(self, *args, **kwargs):
        return self.function(*args, **kwargs)

    def for_json(self):
        return {'__cls__': 'FakePickleLambda', 'faker': self.operation_descriptor}

    def __getstate__(self):
        return {'faker': self.operation_descriptor}

    def __repr__(self):
        return self.operation_descriptor


def die_range(num):
    """Return values for die with sides from 1 to ``num``."""
    return range(1, num+1)


@profile
def slow_simple_rolls(num_dice, die_sides=6, ignore_cache=False):
    """Return all possible roll results for ``num_dice``d``die_sides``."""
    key = (num_dice, die_sides)
    if key not in _rolls_cache or ignore_cache:
        die_vals = np.arange(1, die_sides+1, dtype=_die_dtype)
        result = cartesian([die_vals]*num_dice)
        result.flags.writeable = False
        _rolls_cache[key] = result
    else:
        result = _rolls_cache[key]

    return result


@profile
def simple_rolls(num_dice, die_sides=6, ignore_cache=False):
    """Return all possible roll results for ``num_dice``d``die_sides``.

    Note: return results just like ``slow_simple_rolls`` but works about 50 times faster,
    and does not use recursive ``cartesian`` function

    """
    key = (num_dice, die_sides)
    if (key not in _rolls_cache) or ignore_cache:
        die_vals = np.arange(1, die_sides+1, dtype=_die_dtype)
        num_results = array_prod(np.array([die_sides]*num_dice, dtype=_die_dtype))
        result = np.zeros((num_results, num_dice), dtype=_die_dtype)

        for depth in range(num_dice):
            segment_len = int(
                num_results / array_prod(np.array([die_sides]*(depth+1), dtype=_die_dtype))
            )

            shift = 0
            while (shift+segment_len) <= num_results:
                for die_val in die_vals:
                    segment_start = (die_val-1)*segment_len + shift
                    semgent_end = segment_start + segment_len + shift
                    result[segment_start:semgent_end, depth] = die_val
                shift += segment_len*die_sides

        result.flags.writeable = False
        _rolls_cache[key] = result
    else:
        result = _rolls_cache[key]

    return result


def num_permutations(*obj_nums):
    """Return number of permutations with repetitions of indistinguishable objects.

    each argument is a number of objects of one style.
    """
    if len(obj_nums) == 1:
        return 1

    obj_nums = np.asarray(obj_nums, dtype=_die_dtype)
    obj_nums.sort()
    total_objects = array_sum(obj_nums)
    max_num = obj_nums[-1]

    numerator = np.prod(np.arange(max_num+1, total_objects+1, dtype=_die_dtype))
    denominator = np.prod(np.concatenate(
        tuple(
            np.arange(1, num+1, dtype=_die_dtype) for num in obj_nums[:-1]
        )
    ))
    return numerator/denominator


@profile
def one_pass_explode(base_rolls, num_dice, die_sides, explode_condition=None, ignore_explodes_in_cols_up_to=None, ignore_cache=False):
    results = None

    base_vert_size, base_horiz_size = base_rolls.shape

    horiz_size = base_horiz_size*2
    total_vert_size = 0

    have_exploded = explode_condition(base_rolls)

    if ignore_explodes_in_cols_up_to >= 0:
        have_exploded[:, :ignore_explodes_in_cols_up_to+1] = False
        horiz_size = horiz_size - ignore_explodes_in_cols_up_to - 1

    explodable_dice = horiz_size - base_horiz_size

    explodes_count = array_sum(np.rint(have_exploded), axis=1)

    data_by_explode_count = {}

    for num_exploded in range(0, explodable_dice+1):
        chose_condition = (explodes_count == num_exploded)
        chosen = np.compress(chose_condition, base_rolls, axis=0)

        explosion_size = die_sides**num_exploded
        data_by_explode_count[num_exploded] = (explosion_size, chosen)
        total_vert_size += chosen.shape[0] * explosion_size

    results = np.zeros((total_vert_size, horiz_size), dtype=_die_dtype)
    current_row = 0

    for num_exploded, (explosion_size, exploded_rolls) in data_by_explode_count.items():
        if not num_exploded:
            end_row = current_row + exploded_rolls.shape[0]
            results[current_row:end_row, :exploded_rolls.shape[1]] = exploded_rolls
            current_row = end_row
        else:
            extra_rolls = simple_rolls(num_exploded, die_sides)
            for rolls_option in exploded_rolls:
                end_row = current_row + explosion_size
                results[current_row:end_row, :base_horiz_size] = rolls_option
                results[current_row:end_row, base_horiz_size:base_horiz_size+num_exploded] = extra_rolls
                current_row = end_row

    return results



_multi_pass_exploding_rolls_cache = {}


@profile
def multi_pass_exploding_rolls(num_dice, die_sides, explode_condition=None, explode_depth=4, ignore_cache=False):
    die_vals = np.arange(1, die_sides+1, dtype=_die_dtype)
    if explode_condition is None:
        explode_on = tuple()
    else:
        explode_on = tuple(die_vals[explode_condition(die_vals)])

    cache_key = (num_dice, die_sides, explode_on, explode_depth)

    if (cache_key not in _multi_pass_exploding_rolls_cache) or ignore_cache:
        results = simple_rolls(num_dice, die_sides, ignore_cache=ignore_cache)
        if not explode_on:
            return results

        prev_results = np.ndarray((0, 0))

        for i in range(explode_depth):
            prev_num_cols = prev_results.shape[1]
            prev_results = results
            results = one_pass_explode(results, num_dice, die_sides, explode_condition, ignore_explodes_in_cols_up_to=prev_num_cols-1, ignore_cache=ignore_cache)

        _multi_pass_exploding_rolls_cache[cache_key] = results
    else:
        results = _multi_pass_exploding_rolls_cache[cache_key]

    return results


def calc_probabilities(dice_results):
    results = 1/6**array_sum(np.rint(dice_results >= 1, dtype=np.float64), axis=1)
    return results


_hints_cache = {}


def get_hints(values_list):
    """Get hints about values_list.

    Right now returns 2 or 4  hints:
        * 'is_range', 'num_items'
        * 'range_start', 'range_end' (inclusive) when it's a range
    """
    if isinstance(values_list, tuple):
        key = values_list
    elif isinstance(values_list, np.ndarray):
        key = values_list.tostring()
    else:
        key = tuple(values_list)
    if key not in _hints_cache:
        num_items = len(values_list)
        hints = {'is_range': False, 'num_items': num_items}
        if num_items > 1:
            if isinstance(values_list, np.ndarray):
                sorted_list = np.sort(values_list)
                is_range = (sorted_list == np.arange(sorted_list[0], sorted_list[-1]+1)).all()
            else:
                sorted_list = sorted(values_list)
                is_range = (sorted_list == range(sorted_list[0], sorted_list[-1]+1))
            if is_range:
                hints['is_range'] = True
                hints['range_start'] = sorted_list[0]
                hints['range_end'] = sorted_list[-1]
        _hints_cache[key] = hints
    else:
        hints = _hints_cache[key]
    return hints


def get_filter_by_list(array, values_list, hints=None, axis=1):
    """Return 4 arrays: one with rows from ``array`` containing anything from values_list and second that does not cantain."""
    if hints is None:
        hints = get_hints(values_list)
    if 'num_items' not in hints:
        hints['num_items'] = len(values_list)
    if ('is_range' not in hints) or not ('range_start' in hints and 'range_end' in hints):
        hints.update(get_hints(values_list))

    filter_array = np.empty(0, dtype=np.bool)
    if hints['num_items'] == 1:
        filter_array = np.equal(array, values_list[0])
        if axis is not None:
            filter_array = filter_array.any(axis=axis)
    elif hints['num_items']:
        if hints['is_range']:
            gte_filter = np.greater_equal(array, hints['range_start'])
            if axis is not None:
                gte_filter = gte_filter.any(axis=axis)
            lte_filter = np.less_equal(array, hints['range_end'])
            if axis is not None:
                lte_filter = lte_filter.any(axis=axis)
            filter_array = gte_filter & lte_filter
        else:
            if axis == 1:
                shp = (array.shape[0],)
            elif axis == 0:
                shp = (array.shape[1],)
            elif axis is None:
                shp = array.shape
            else:
                raise ValueError('axis should be 1, 0 or None')
            filter_array = np.full(shp, False, dtype=np.bool)
            for val in values_list:
                extra_filter = np.equal(array, val)
                if axis is not None:
                    extra_filter = filter_array.any(axis=axis)
                filter_array |= extra_filter

    return filter_array


@profile
def reroll_rolls(num_dice, die_sides, results, probs, kept_results, reroll_on, reroll_hints=None, rerolls_to_keep=None):
    """Roll additional dice if reroll_on "intersects" with items in results.

    Note: this is a most time-consuming functions: around 80-90% of ``complex_rolls``
    function is spent here.

    """
    if reroll_hints is None:
        reroll_hints = get_hints(reroll_on)
    if rerolls_to_keep is None:
        rerolls_to_keep = []

    results = np.asarray(results)
    probs = np.asarray(probs)
    reroll_on = np.asarray(reroll_on)
    rerolls_to_keep = np.asarray(rerolls_to_keep)

    rerolls_filter = get_filter_by_list(results, reroll_on, reroll_hints)
    inv_filter = ~rerolls_filter
    rerolls_to_not_keep = reroll_on[~np.in1d(reroll_on, rerolls_to_keep)]

    rerolled, not_rerolled = results[rerolls_filter], results[inv_filter]
    rerolled_probs, not_rerolled_probs = probs[rerolls_filter], probs[inv_filter],

    result_parts = [not_rerolled]
    result_probs = [not_rerolled_probs]

    max_result_dice = not_rerolled.shape[1]

    if kept_results is None:
        kept_results = np.empty((not_rerolled.shape[0], 0))
        prev_kepts = np.empty((rerolled.shape[0], 0))
    else:
        prev_kepts = kept_results[rerolls_filter]
        kept_results = kept_results[inv_filter]

    max_kept_result_dice = kept_results.shape[1]

    kept_results_parts = [kept_results]

    for rerollable, prob, prev_kept in zip(rerolled, rerolled_probs, prev_kepts):
        rerolled_vals = rerollable[np.in1d(rerollable, reroll_on)]
        num_rerolled = len(rerolled_vals)

        kept_vals = rerollable[~np.in1d(rerollable, rerolls_to_not_keep)]
        kept_vals = np.hstack((kept_vals, prev_kept))

        new_prob = prob/(die_sides**num_rerolled)

        result_part = simple_rolls(num_rerolled, die_sides)
        kept_result_part = np.tile(kept_vals, (result_part.shape[0], 1))

        kept_results_parts.append(kept_result_part)
        result_probs.append(np.full((result_part.shape[0]), new_prob, dtype=probs.dtype))
        result_parts.append(result_part)

        max_result_dice = max(result_part.shape[1], max_result_dice)
        max_kept_result_dice = max(kept_result_part.shape[1], max_kept_result_dice)

    # "resize" all rasults parts to have the same number of columns
    resized_result_parts = []
    for result_part in result_parts:
        zeros_size = max_result_dice - result_part.shape[1]
        if zeros_size:
            result_part = np.hstack([
                result_part,
                np.zeros((result_part.shape[0], zeros_size))
            ])
        resized_result_parts.append(result_part)

    resized_kept_results = []
    for kept_results_part in kept_results_parts:
        zeros_size = max_kept_result_dice - kept_results_part.shape[1]
        if zeros_size:
            kept_results_part = np.hstack([
                kept_results_part,
                np.zeros((kept_results_part.shape[0], zeros_size))
            ])
        resized_kept_results.append(kept_results_part)

    return np.concatenate(resized_result_parts), np.concatenate(result_probs), np.concatenate(resized_kept_results)


def explode_rolls(num_dice, die_sides, results, probs, kept_results, explode_on, reroll_hints=None):
    return reroll_rolls(num_dice, die_sides, results, probs, kept_results, explode_on, reroll_hints, explode_on)

_complex_rolls_cache = {}


@profile
def complex_rolls(
        num_dice, die_sides=6, explode_on=None, reroll_on=None,
        explode_depth=3, reroll_depth=1, hints=None, operation_order=None, ignore_cache=False):
    """Get stats for complex rolled dice.

    Return 2 arrays:
        * 2 dimensional array where each row is some possible result of dice rolling
        * 1 dimensional array with "probability-multiplier" for corresponding row in previous array.

    Rolling order:
    1. Roll
    2. if reroll_depth >= 1: reroll, decrement reroll_depth
    3. if explode_depth >= 1: explode, decrement explode_depth
    4. if reroll_depth or explode_depth: goto 2

    Warning!
    As you can see in "rolling order" rerolls are performed before explodes so
    if your reroll_depth is set to 1 then exploded results won't reroll.
    If reroll_depth is se to 2 you will get rerolls of explodes but already
    rerolled values WILL reroll too.

    You can change default order of reroll/explode by using ``opration_order = ['explode', 'reroll']``

    """
    if operation_order is None:
        operation_order = ('reroll', 'explode')
    else:
        operation_order = tuple(operation_order)

    explode_on = np.asarray(explode_on or tuple())
    reroll_on = np.asarray(reroll_on or tuple())

    key = (
        num_dice, die_sides, tuple(explode_on), tuple(reroll_on),
        explode_depth, reroll_depth, operation_order
    )
    if (key not in _complex_rolls_cache) or ignore_cache:
        results = simple_rolls(num_dice, die_sides)
        probs = np.ones(results.shape[0], dtype=np.float64)
        hints = hints or {}
        kept_results = None
        explode_hints = hints.get('explode', None) or get_hints(explode_on)
        reroll_hints = hints.get('reroll', None) or get_hints(reroll_on)

        while (explode_depth and explode_on) or (reroll_depth and reroll_on):
            for operation in operation_order:
                if operation == 'reroll' and reroll_on and reroll_depth > 0:
                    results, probs, kept_results = reroll_rolls(num_dice, die_sides, results, probs, kept_results, reroll_on, reroll_hints)
                    reroll_depth -= 1
                if operation == 'explode' and explode_on and explode_depth > 0:
                    results, probs, kept_results = explode_rolls(num_dice, die_sides, results, probs, kept_results, explode_on, explode_hints)
                    explode_depth -= 1

        if kept_results is not None:
            results = np.hstack((kept_results, results))

        _complex_rolls_cache[key] = results, probs
    else:
        results, probs = _complex_rolls_cache[key]
    return results, probs


@profile
def np_condition_probability(rolls_results_array, probs_array, condition, fraction=False):
    passes_filter = condition(rolls_results_array)
    passes_probs = probs_array[passes_filter]
    return array_sum(passes_probs) / array_sum(probs_array)


@profile
def reduce_rolls(rolls_results, func, like_map=False):
    """Reduce ``rolls_results`` using ``func``.

    :param bool like_map: apply function to each element of rolls_results.

    :param object func:

        Object that is used to process rolls.

        if object has ``reduce`` method (like :py:method:`numpy.ufunc.reduce`) than it's called like::

            func.reduce(rolls_results, axis=1)

        otherwise:

            * If ``bool(like_map) is True``::

                rolls_results = [func(rolls_result_item) for rolls_result_item in rolls_results]

            * If ``bool(like_map) is False`` this method tries 2 call signatures (in order)
              stopping on first success::

                  rolls_results = func(rolls_results, axis=1)
                  rolls_results = func(rolls_results)


    """
    if hasattr(func, 'reduce'):
        rolls_results = func.reduce(rolls_results, axis=1)
    else:
        if like_map:
            rolls_results = np.asarray([func(rolls_result_item) for rolls_result_item in rolls_results])
        else:
            try:
                rolls_results = func(rolls_results, axis=1)
            except Exception:
                rolls_results = func(rolls_results)
    return rolls_results


_reduced_array_results_cache = {}


def get_reduced_array_results(num_dice, func, die_sides=None, like_map=False, **kwargs):
    if getattr(func, 'operation_descriptor', ''):
        key = (
            num_dice,
            func.operation_descriptor,
            die_sides,
            like_map,
            tuple((key, tuple(val)) if isinstance(val, list) else (key, val) for (key, val) in kwargs.items())
        )
    else:
        key = None

    if key and key in _reduced_array_results_cache:
        rolls_results, probs = _reduced_array_results_cache[key]
    else:
        rolls_results, probs = complex_rolls(num_dice, die_sides, **kwargs)
        rolls_results = reduce_rolls(rolls_results, func, like_map)
        _reduced_array_results_cache[key] = rolls_results, probs
    return rolls_results, probs


def on_reduced_probability(num_dice, reducer, condition, die_sides=None, like_map=False, **kwargs):
    """Calculate ratio of rolls where result reduced with 'reducer' satisfies 'condition'.

    :param int num_dice:

    :param callable reducer:
        callable that returns results for a roll. Must accept single argument with list of rolled
        values. Most rolls use sum.

    :param callable condition:
        callable that processes all roll variations and leaves only rolls that should contribute
        to probability.

        * to get value for probability density function (PDF) at point X:
        ``condition = lambda roll_result: roll_result == X``

        * to get value for cumulative distribution function (CDF) at point X:
        ``condition = lambda roll_result: roll_result <= X``

        If callable is `pure <https://en.wikipedia.org/wiki/Pure_function>`_ and
        results may be cached, then it's adviced to set ``operation_descriptor`` attribute
        on that callable that consistently and uniquely describes it's behaviour.
        Example::

            condition = lambda roll_result: res == 6
            condition.operation_descriptor = 'val==6'

    :param bool like_map: this argument is passed to :py:func:`reduce_rolls`

    """
    if die_sides is None:
        die_sides = 6
    rolls_results, probs = get_reduced_array_results(num_dice, reducer, die_sides, like_map=like_map, **kwargs)
    return np_condition_probability(rolls_results, probs, condition)


def get_sum_condition_func(condition_on_sum, operation_descriptor=None):
    """Return callable that applies ``condition_on_sum(sum(x))`` on input.

    Callable is suitable for use in :py:func:`on_reduced_probability`.

    """
    if operation_descriptor is None:
        operation_descriptor = repr(condition_on_sum)

    sum_condition = lambda l: condition_on_sum(sum(l))
    set_operation_descriptor(sum_condition, fallback=operation_descriptor)
    return sum_condition


def get_count_condition_func(condition_to_count, condition_on_count, operation_descriptor=None):
    """Return callable that filters input using ``condition_to_count`` and then applies ``condition_on_count(len(filtered_input))``.

    Callable is suitable for use in :py:func:`on_reduced_probability`.

    """
    if operation_descriptor is None:
        operation_descriptor = '%s, %s' % (repr(condition_to_count), repr(condition_on_count))

    count_condition = lambda l: condition_on_count(len(list(filter(condition_to_count, l))))
    set_operation_descriptor(count_condition, fallback=operation_descriptor)
    return count_condition


def sum_condition_probability(condition_on_sum, operation_descriptor=None, *args, **kwargs):
    """Calculate ratio of rolls where sum on dice satisfy condition."""
    if operation_descriptor is None:
        operation_descriptor = 'sum,%s' % repr(condition_on_sum)

    set_operation_descriptor(condition_on_sum, operation_descriptor)

    if 'roll_results' in kwargs:
        roll_results = kwargs['roll_results']
        probs_array = kwargs['probs']
        return np_condition_probability(array_sum(roll_results, axis=1), probs_array, condition_on_sum)
    else:
        kwargs['condition'] = condition_on_sum
        kwargs['reducer'] = array_sum
        return on_reduced_probability(*args, **kwargs)


def count_condition_probability(condition_to_count, condition_on_count, condition_to_count_operation_descriptor, condtition_on_count_operation_descriptor, *args, **kwargs):
    """Calculate ratio of rolls where count of results filtered by ``condition_to_count`` satisfy ``condition_on_count``."""
    kwargs['reducer'] = lambda data: array_sum(np.rint(condition_to_count(data)), axis=1)
    set_operation_descriptor(kwargs['reducer'], condition_to_count_operation_descriptor)

    kwargs['condition'] = condition_on_count
    set_operation_descriptor(kwargs['condition'], condtition_on_count_operation_descriptor)

    return on_reduced_probability(*args, **kwargs)


def get_throw_sum_probabilities(num_dice, die_sides, kept_largest=None, kept_least=None):
    """Method to get probabilities for most commonly used throws: sum of XdYkN.

    :param int num_dice: number of dice

    :param int die_sides: number of sides on a die

    :param int kept_largest: number of best results kept (all by default)
    :param int kept_least: number of worst results kept (all by default)

    """
    if kept_largest is None and kept_least is None:
        kept_largest = num_dice

    if kept_largest is not None and kept_least is not None:
        raise TypeError("'kept_largest' and 'kept_least' options are mutually exclusive")
    elif (0 in (kept_largest, kept_least)) or (kept_largest and kept_largest < 0) or (kept_largest and kept_largest < 0):
        raise ValueError("'kept_largest' or 'kept_least' must be a positive integer")

    def get_cond(val):
        cond = lambda res: res == val
        cond.operation_descriptor = 'val==%s' % val

    roll_vals = range(num_dice, num_dice*die_sides+1)
    dice_stats = []

    if kept_largest:
        reducer = lambda rolls: sum(sorted(rolls, reverse=True)[:kept_largest])
        reducer.operation_descriptor = 'sum(largest=%s)' % kept_largest
    elif kept_least:
        reducer = lambda rolls: sum(sorted(rolls)[:kept_least])
        reducer.operation_descriptor = 'sum(least=%s)' % kept_least
    else:
        reducer = array_sum

    for val in roll_vals:
        dice_stats.append(on_reduced_probability(
            num_dice,
            lambda rolls: sum(sorted(rolls, reverse=True)[:3]),
            get_cond(val),
            like_map=True
        ))


def save_plots():
    try:
        import matplotlib
        import matplotlib.pyplot as plt
        import matplotlib.patheffects as path_effects
        from matplotlib.patches import Rectangle
    except ImportError:
        matplotlib = None

    if not matplotlib:
        sys.exit("Can't plot without matplotlib")
    else:
        percent_formatter = lambda val: ('%.10f' % (val*100)).rstrip('0').strip('.') + '%'
        colors = itertools.cycle([
            "#619CFF", "#6BB100",
            "#E58700", "#A3A500", "#00C0AF",
            "#FF67A4", "#B983FF",
            # "#C99800", "#00BA38", "#E58700",
        ])

        def draw_plots(x, y, extra_skew=0, color=None, width=0.125, with_vals=False, x_vals_skew=0.2, y_vals_skew=0.004, vals_step=1, text_size='small', **kwargs):
            filler_len = 3
            new_x = tuple(np.arange(x[0]-filler_len, x[0])) + tuple(x) + tuple(np.arange(x[-1]+1, x[-1]+filler_len+1))
            new_y = tuple(reversed([-v for v in y[:filler_len]])) + tuple(y) + tuple(reversed([-v for v in y[-filler_len:]]))
            new_x = np.array(new_x)
            new_y = np.array(new_y)

            x = [v-extra_skew for v in x]
            plt.bar(x, y, width=width, alpha=1, align='center', color=color)
            plt.plot(new_x-extra_skew, new_y, '--', color=color, **kwargs)

            if with_vals:
                for x_val, y_val in zip(x, y):
                    if (x_val+extra_skew) % vals_step != 0:
                        continue
                    if y_val >= 0.0003:
                        if y_val >= 0.1:
                            text = '%d%%' % (y_val * 100)
                        elif y_val >= 0.01:
                            text = '%.1f%%' % (y_val * 100)
                        else:
                            text = '%.2f%%' % (y_val * 100)
                        y_val_coeff = 0.98
                        text = plt.text(x_val+x_vals_skew+extra_skew, y_val*y_val_coeff+y_vals_skew, text, color=color, size=text_size,)
                        text.set_path_effects([path_effects.Stroke(linewidth=2, foreground='white'), path_effects.Normal()])

        # plt.ion()
        plt.style.use('ggplot')

        if True:
            # compare complex rolls vs multi_pass_exploding_rolls
            for i in range(3):
                multi_pass_exploding_rolls(3, 6, lambda x: x == 6, 3, ignore_cache=True)
                complex_rolls(3, 6, [6], None, 3, ignore_cache=True)

        # Xd6 stats
        if False:
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xticks(range(20))
            ax.set_xlim(0, 20)
            ax.set_ylim(0, 0.2)

            # 2d6:
            list_2d6_vals = range(2, 12+1)
            list_2d6_rolls = simple_rolls(2)
            list_2d6_stats = []
            get_cond = lambda val: lambda sum: sum == val
            get_descr = lambda val: ('sum==%s' % val)

            for val in list_2d6_vals:
                list_2d6_stats.append(sum_condition_probability(get_cond(val), get_descr(val), num_dice=2))

            draw_plots(list_2d6_vals, list_2d6_stats, 0.125, next(colors), label='2d6', with_vals=True)

            # 3d6:
            list_3d6_vals = range(3, 18+1)
            list_3d6_rolls = simple_rolls(3)
            list_3d6_stats = []
            for val in list_3d6_vals:
                list_3d6_stats.append(sum_condition_probability(get_cond(val), get_descr(val), num_dice=3))

            draw_plots(list_3d6_vals, list_3d6_stats, 0, next(colors), label='3d6', with_vals=True)

            # chain exploding 1d6
            kwargs = {'explode_on': [6], 'explode_depth': 3}

            exploding_1d6_vals = range(1, 6*3+1)
            exploding_1d6_stats = []
            for val in exploding_1d6_vals:
                exploding_1d6_stats.append(sum_condition_probability(get_cond(val), get_descr(val), num_dice=1, **kwargs))

            draw_plots(exploding_1d6_vals, exploding_1d6_stats, 0.25, next(colors), label='1d6 with explodes', with_vals=True)

            # 4d6k3 (roll 4d6, keep 3 best):
            get_cond = lambda val: PicklableLambda(lambda res: res == val, 'val==%s' % val)
            list_4d6k3_vals = range(3, 18+1)
            list_4d6k3_stats = []
            for val in list_4d6k3_vals:
                list_4d6k3_stats.append(on_reduced_probability(
                    4,
                    lambda rolls: sum(sorted(rolls, reverse=True)[:3]),
                    get_cond(val),
                    like_map=True
                ))

            draw_plots(list_4d6k3_vals, list_4d6k3_stats, -0.125, next(colors), label='4d6k3', with_vals=True)

            handles, labels = ax.get_legend_handles_labels()
            ax.legend(handles, labels)

            plt.savefig('Xd6_stats.png', bbox_inches='tight', dpi=117)
            plt.clf()

        # alone 4d6
        if True:
            cur_color = next(colors)
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xticks(range(20))
            ax.set_xlim(0, 20)

            get_cond = lambda val: PicklableLambda(lambda res: res == val, 'val==%s' % val)
            list_4d6k3_vals = range(3, 18+1)
            list_4d6k3_stats = []
            for val in list_4d6k3_vals:
                list_4d6k3_stats.append(on_reduced_probability(
                    4,
                    lambda rolls: sum(sorted(rolls, reverse=True)[:3]),
                    get_cond(val),
                    like_map=True
                    )
                )

            ax.set_ylim(0, 0.15)
            ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda y, pos: percent_formatter(y)))
            draw_plots(list_4d6k3_vals, list_4d6k3_stats, 0, cur_color, label='4d6k3')
            average = 0
            sigma = 0
            for (x, y) in zip(list_4d6k3_vals, list_4d6k3_stats):
                average += x*y
                if y >= 0.03:
                    text = '%d%%' % (y * 100)
                elif y >= 0.01:
                    text = '%.1f%%' % (y * 100)
                else:
                    text = '%.2f%%' % (y * 100)
                text = plt.text(x-0.4, y*0.98+0.004, text, size='small',)
                text.set_path_effects([path_effects.Stroke(linewidth=2, foreground='white'), path_effects.Normal()])
            for (x, y) in zip(list_4d6k3_vals, list_4d6k3_stats):
                sigma += y*(x-average)**2
            sigma = sigma**0.5

            left_sigma = average-sigma
            left_y = sum(list_4d6k3_stats[6:7])*1.1 / 0.15
            right_sigma = average+sigma
            right_y = sum(list_4d6k3_stats[12:13]) / 0.15

            mid_y = sum(list_4d6k3_stats[9:10]) / 0.15

            for y, x, text in ((left_y, left_sigma, '$\mu-\sigma$'), (mid_y, average, '$\mu$'), (right_y, right_sigma, '$\mu+\sigma$')):
                plt.axvline(x, 0, y, zorder=1)
                text = plt.text(x+0.2, 0.005, text)
                text.set_path_effects([path_effects.Stroke(linewidth=2, foreground='white'), path_effects.Normal()])

            handles, labels = ax.get_legend_handles_labels()
            extra_handle = Rectangle((0, 0), 0, 0, alpha=0.0)
            handles += [extra_handle, extra_handle]
            labels.append('$\mu=%.1f$' % average)
            labels.append('$\sigma=%.1f$' % sigma)

            ax.legend(handles, labels)
            plt.savefig('4d6k3_stats.png', bbox_inches='tight', dpi=117)

        # dnd advantage/disadvantage: sum(min(2d20)), sum(max(2d20))
        if True:
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlim(0, 21)
            ax.set_ylim(0, 0.15)
            ax.set_xticks(range(22))
            ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda y, pos: percent_formatter(y)))

            min_reducer = lambda data: np.min(data, axis=1)
            max_reducer = lambda data: np.max(data, axis=1)

            get_cond = lambda val: PicklableLambda(lambda res: res == val, 'val==%s' % val)

            kwargs = {}
            max_depth = 3

            list_1d20_vals = die_range(20)

            list_min_2d20_stats = []
            min_average = 0
            max_average = 0
            list_max_2d20_stats = []
            list_max_3d20_stats = []
            max_3_average = 0
            for val in list_1d20_vals:
                y1 = on_reduced_probability(
                    2, min_reducer, get_cond(val),
                    die_sides=20,
                    **kwargs
                )
                list_min_2d20_stats.append(y1)
                min_average += y1*val
                y2 = on_reduced_probability(
                    2, max_reducer, get_cond(val),
                    die_sides=20,
                    **kwargs
                )
                list_max_2d20_stats.append(y2)
                max_average += y2*val

                y3 = on_reduced_probability(
                    3, max_reducer, get_cond(val),
                    die_sides=20,
                    **kwargs
                )
                list_max_3d20_stats.append(y3)
                max_3_average += y3*val

            draw_plots(list_1d20_vals, list_min_2d20_stats, -0.2, next(colors), width=0.2, label='min(2d20)', with_vals=True, x_vals_skew=-0.1, y_vals_skew=0.001)
            draw_plots(list_1d20_vals, list_max_2d20_stats, 0.0, next(colors), width=0.2, label='max(2d20)', with_vals=True, x_vals_skew=0.2, y_vals_skew=0.005)
            draw_plots(list_1d20_vals, list_max_3d20_stats, 0.2, next(colors), width=0.2, label='max(3d20)', with_vals=True, x_vals_skew=-0.4, y_vals_skew=0.004)

            handles, labels = ax.get_legend_handles_labels()
            extra_handle = Rectangle((0, 0), 0, 0, alpha=0.0)
            handles = [handles[0], extra_handle, handles[1], extra_handle, handles[2], extra_handle]
            labels = [labels[0], '$\mu=%.1f$' % min_average, labels[1], '$\mu=%.1f$' % max_average, labels[2], '$\mu=%.1f$' % max_3_average]
            ax.legend(handles, labels, loc=9)

            plt.savefig('min_max_2d20_stats.png', bbox_inches='tight', dpi=117)

        # dnd halflings: 1d20, reroll 1 once
        if True:
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlim(0, 21)
            ax.set_ylim(0, 0.07)
            ax.set_xticks(range(22))
            ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda y, pos: percent_formatter(y)))

            reducer = lambda data: data.reshape((data.shape[0], ))
            get_cond = lambda val: PicklableLambda(lambda res: res == val, 'val==%s' % val)

            kwargs = {'reroll_on': [1], 'reroll_depth': 1}
            max_depth = 1

            list_1d20_vals = die_range(20)

            list_lucky_1d20_stats = []
            average = 0
            list_lucky_1d20_stats = []
            for val in list_1d20_vals:
                y = on_reduced_probability(
                    1, reducer, get_cond(val),
                    die_sides=20,
                    **kwargs
                )
                list_lucky_1d20_stats.append(y)
                average += y*val

            draw_plots(list_1d20_vals, list_lucky_1d20_stats, -0.1, next(colors), width=0.2, label='lucky 1d20', with_vals=True, y_vals_skew=0.001)

            handles, labels = ax.get_legend_handles_labels()
            extra_handle = Rectangle((0, 0), 0, 0, alpha=0.0)
            handles = [handles[0], extra_handle, ]
            labels = [labels[0], '$\mu=%.1f$' % average, ]
            ax.legend(handles, labels)

            plt.savefig('lucky_1d20_stats.png', bbox_inches='tight', dpi=117)

        # dnd halfling advantage/disadvantage: sum(min(2d20)), sum(max(2d20))
        if True:
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlim(0, 21)
            ax.set_ylim(0, 0.105)
            ax.set_xticks(range(22))
            ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda y, pos: percent_formatter(y)))

            min_reducer = lambda data: np.min(data, axis=1)
            max_reducer = lambda data: np.max(data, axis=1)

            get_cond = lambda val: PicklableLambda(lambda res: res == val, 'val==%s' % val)

            kwargs = {'reroll_on': [1], 'reroll_depth': 1}
            max_depth = 3

            list_1d20_vals = die_range(20)

            list_min_2d20_stats = []
            min_average = 0
            max_average = 0
            list_max_2d20_stats = []
            for val in list_1d20_vals:
                y1 = on_reduced_probability(
                    2, min_reducer, get_cond(val),
                    die_sides=20,
                    **kwargs
                )
                list_min_2d20_stats.append(y1)
                min_average += y1*val
                y2 = on_reduced_probability(
                    2, max_reducer, get_cond(val),
                    die_sides=20,
                    **kwargs
                )
                list_max_2d20_stats.append(y2)
                max_average += y2*val

            draw_plots(list_1d20_vals, list_min_2d20_stats, -0.1, next(colors), width=0.2, label='lucky min(2d20)', with_vals=True, y_vals_skew=0.001)
            draw_plots(list_1d20_vals, list_max_2d20_stats, 0.1, next(colors), width=0.2, label='lucky max(2d20)', with_vals=True, y_vals_skew=0.001)

            handles, labels = ax.get_legend_handles_labels()
            extra_handle = Rectangle((0, 0), 0, 0, alpha=0.0)
            handles = [handles[0], extra_handle, handles[1], extra_handle]
            labels = [labels[0], '$\mu=%.1f$' % min_average, labels[1], '$\mu=%.1f$' % max_average]
            ax.legend(handles, labels, loc=9)

            plt.savefig('lucky_min_max_2d20_stats.png', bbox_inches='tight', dpi=117)

        # nWoD: Xd10>=8! (count dice with values >=8, explode tens)
        if True:
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlim(-0.3, 12)
            ax.set_xticks(range(13))
            ax.set_yscale('log')
            ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda y, pos: percent_formatter(y)))
            # ax.yaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter('%d'))
            # ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda x, pos: str(int(round(x)))))

            reducer = lambda data: array_sum(np.rint(data >= 8), axis=1)
            reducer.operation_descriptor = 'len(rolls>=8)'

            get_cond = lambda val: PicklableLambda(lambda x: x == val, 'x==%s' % val)

            averages = []

            for x in range(1, 6):
                cur_color = next(colors)
                if x >= 6:
                    break
                if x >= 5:
                    max_depth = 2
                elif x >= 4:
                    max_depth = 3
                else:
                    max_depth = 4

                kwargs = {'explode_on': [10], 'explode_depth': max_depth}
                exploding_Xd10_successes = range(0, 11)
                exploding_Xd10_stats = []
                average = 0

                for val in exploding_Xd10_successes:
                    y = on_reduced_probability(
                        x, reducer, get_cond(val), die_sides=10,
                        **kwargs
                    )
                    exploding_Xd10_stats.append(y)
                    average += val*y
                    if y >= 0.0003:
                        if y >= 0.03:
                            text = '%d%%' % (y * 100)
                        elif y >= 0.01:
                            text = '%.1f%%' % (y * 100)
                        else:
                            text = '%.2f%%' % (y * 100)
                        if val == 0:
                            y_coeff = (0.8**x)
                        elif val == 1:
                            y_coeff = (0.74**(4-x))
                        else:
                            y_coeff = 0.83
                        text = plt.text(val+0.3, y*y_coeff, text, color=cur_color, size='small',)
                        text.set_path_effects([path_effects.Stroke(linewidth=2, foreground='white'), path_effects.Normal()])
                averages.append(average)

                draw_plots(exploding_Xd10_successes, exploding_Xd10_stats, 0.2-0.1*(x-1), cur_color, width=0.07, label='%sd10' % x)

            handles, labels = ax.get_legend_handles_labels()
            extra_handle = Rectangle((0, 0), 0, 0, alpha=0.0)
            new_handles = []
            new_labels = []
            for handle, label, average in zip(handles, labels, averages):
                new_handles += [handle, extra_handle]
                new_labels += [label, ('$\mu=%.2f$' % average)]
            ax.legend(new_handles, new_labels)
            plt.savefig('nwod_stats.png', bbox_inches='tight', dpi=117)

        # DtD: sum(Xd10) with explodes
        if True:
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1)
            ax.set_xlim(0, 56)
            ax.set_ylim(0, 0.12)
            ax.set_xticks(tuple(range(0, 11, 1)) + tuple(range(12, 56, 2)))
            ax.yaxis.set_major_formatter(matplotlib.ticker.FuncFormatter(lambda y, pos: percent_formatter(y)))

            reducer = array_sum

            def get_cond(val):
                condition = lambda x: x == val
                condition.operation_descriptor = 'x==%s' % val
                return condition

            averages = []
            for x in range(1, 5):
                average = 0
                cur_color = next(colors)
                if x >= 6:
                    break
                if x >= 5:
                    max_depth = 2
                elif x >= 4:
                    max_depth = 3
                else:
                    max_depth = 3

                kwargs = {
                    'explode_on': [10], 'explode_depth': max_depth,
                }
                exploding_Xd10_successes = range(x, 56)
                exploding_Xd10_stats = []

                for val in exploding_Xd10_successes:
                    y = on_reduced_probability(x, reducer, get_cond(val), die_sides=10, **kwargs)
                    average += val*y
                    exploding_Xd10_stats.append(y)
                averages.append(average)
                if x <= 2:
                    vals_step = 2
                else:
                    vals_step = 3

                draw_plots(exploding_Xd10_successes, exploding_Xd10_stats, 0.3-0.2*(x-1), cur_color, width=0.2, label='%sd10' % x, with_vals=True, vals_step=vals_step, text_size='x-small')

            handles, labels = ax.get_legend_handles_labels()
            extra_handle = Rectangle((0, 0), 0, 0, alpha=0.0)
            new_handles = []
            new_labels = []
            for handle, label, average in zip(handles, labels, averages):
                new_handles += [handle, extra_handle]
                new_labels += [label, ('$\mu=%.2f$' % average)]
            ax.legend(new_handles, new_labels)

            plt.savefig('sum_exploding_Xd10_stats.png', bbox_inches='tight', dpi=117)
            plt.clf()


if __name__ == '__main__':
    save_plots()
